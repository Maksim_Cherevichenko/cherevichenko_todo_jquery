$(() => {
  const $TaskInput = $('#input-task');
  const $TaskList = $('#list');
  const $AddBtn = $('#add-task');
  const $CheckBtn = $('#select-all');
  const $RmCompleteButton = $('#remove-complete');
  const $ShowComplete = $('#show-complete');
  const $ShowCurrent = $('#show-current');
  const $ShowAll = $('#show-all');
  const $AllCounter = $('#1counter');
  const $CompleteCounter = $('#2counter');
  const $CurrentCounter = $('#3counter');
  const $paginationBlock = $('#pag');
  const $currentDoc = $(document);
  const { _ } = window;
  let todos = [];
  let page = 1;
  const limitPerPage = 4;
  let tabStatus = 'all';

  // counter update
  function counterUpdate() {
    const allTodoCount = todos.length;
    const doneTodosCount = todos.filter((todo) => todo.done).length;
    const activeTodosCount = allTodoCount - doneTodosCount;
    $AllCounter.val(`Всего: ${allTodoCount}`);
    $CompleteCounter.val(`Выполненные: ${doneTodosCount}`);
    $CurrentCounter.val(`Текущие: ${activeTodosCount}`);
  }

  function classficateTabs(target) {
    const $TabContainer = $('.tab-container button');
    const $CurrentTarget = $(target);
    $TabContainer.removeClass('selected');
    $CurrentTarget.addClass('selected');
  }
  // filtered array
  function tabulateArray() {
    let tabulatedArray = [];
    switch (tabStatus) {
      case 'all':
        classficateTabs($ShowAll);
        tabulatedArray = todos;
        break;
      case 'completed':
        classficateTabs($ShowComplete);
        tabulatedArray = todos.filter((todo) => todo.done);
        break;
      case 'current':
        classficateTabs($ShowCurrent);
        tabulatedArray = todos.filter((todo) => !todo.done);
        break;
      default:
        return tabulatedArray;
    }
    return tabulatedArray;
  }

  // count of pages for tabulation
  function countPages(currentPage) {
    if (!currentPage) {
      page = Math.ceil(tabulateArray().length / limitPerPage);
    } else {
      page = currentPage;
    }
  }

  // checking 'select-all' status
  function checkStatus() {
    document.getElementById('select-all').checked = !(todos.length !== todos.filter((task) => task.done).length && todos.length);
  }

  //  draw all tasks
  function drawList() {
    let drawStr = '';
    tabulateArray().slice((page - 1) * limitPerPage, page * limitPerPage).forEach((task) => {
      drawStr += `
        <div class="list-group">
        <div class="to-left">
        <p><li class="li" id="${task.id}"><input type="checkbox" id="bad" class="check-style" ${task.done ? 'checked' : ''}>  ${_.escape(task.text)}<button id="del-btn" class="btn btn-danger">Удалить</button></li></p>
        </div>
        </div>`;
    });
    counterUpdate();
    $TaskList.html(drawStr);

    // add tabulation page
    function addPage() {
      const numberOfItems = tabulateArray().length;
      let paginationBlock = '';
      for (let i = 0; i < (Math.ceil(numberOfItems / limitPerPage)); i += 1) {
        paginationBlock += `<li class='current-page'><a class='page-link ${(i + 1 === page) ? 'active' : ''}' 'href='javascript:void(0)'>${i + 1}</a></li>`;
      }
      $paginationBlock.html(paginationBlock);
    }
    checkStatus();
    addPage();
  }

  //  add task to task-list
  function addTask() {
    tabStatus = 'all';
    if (!$TaskInput.val().trim()) {
      return false;
    }
    const todo = {
      text: $TaskInput.val(),
      id: Date.now(),
      done: false,
    };
    todos.push(todo);
    $TaskInput.val('');
    countPages();
    drawList();
    checkStatus();
    return true;
  }

  // change status of one task
  function changeStatus(liId) {
    todos.forEach((task) => {
      if (task.id === +liId) {
        const currentTask = task;
        currentTask.done = !currentTask.done;
      }
    });
    switch (tabStatus) {
      case 'all':
        break;
      case 'completed':
        if (Math.ceil(todos.filter((task) => task.done === true).length / limitPerPage) < page) {
          page -= 1;
          countPages(page);
        }
        break;
      case 'current':
        if (Math.ceil(todos.filter((task) => task.done === false).length / limitPerPage) < page) {
          page -= 1;
          countPages(page);
        }
        break;
      default:
        return false;
    }
    checkStatus();
    countPages(page);
    drawList();
    return false;
  }

  //  change checkboxes status
  function changeCheckbox() {
    if (todos.filter((task) => task.done === true).length !== todos.length) {
      todos.forEach((task) => {
        const currentTask = task;
        currentTask.done = true;
      });
    } else {
      todos.forEach((task) => {
        const currentTask = task;
        currentTask.done = false;
      });
    }
    countPages(page);
    drawList();
  }

  //  remove COMPLETE tasks from task-list and array
  function removeCompleteTask() {
    todos = todos.filter((task) => !task.done);
    checkStatus();
    drawList();
  }

  //  remove ONE task from task-list and from array
  $currentDoc.on('click', '#del-btn', function removeOneTask() {
    const currentId = $(this).closest('li').attr('id');
    const index = todos.findIndex((task) => task.id === Number(currentId));
    todos.splice(index, 1);
    switch (tabStatus) {
      case 'all':
        if (Math.ceil(todos.length / limitPerPage) < page) {
          page -= 1;
          countPages(page);
        }
        break;
      case 'completed':
        if (Math.ceil(todos.filter((task) => task.done === true).length / limitPerPage) < page) {
          page -= 1;
          countPages(page);
        }
        break;
      case 'current':
        if (Math.ceil(todos.filter((task) => task.done === false).length / limitPerPage) < page) {
          page -= 1;
          countPages(page);
        }
        break;
      default:
        return false;
    }
    checkStatus();
    drawList();
    return false;
  });

  //  edit task
  $currentDoc.on('dblclick', '.li', (event) => {
    const id = +event.target.id;
    const $selectedTask = todos.filter((task) => task.id === id);
    const drawInput = `<div id="temp"><input type="text" id="new-input" value="${$selectedTask[0].text}" class="form-control" aria-describedby="basic-addon1"><button id="ok">ОК</button><button id="no" class="">ОТМЕНА</button></div>`;
    $(`#${id}`).html(drawInput);
    const $EditInput = $('#new-input');
    const $OkButton = $('#ok');
    const $NoButton = $('#no');
    $EditInput.on('keypress', (e) => {
      if (e.key === 'Enter') {
        todos.forEach((task) => {
          if (task.id === +id && $EditInput.val().trim()) {
            const currentTask = task;
            currentTask.text = $EditInput.val();
          }
          drawList(todos);
        });
      }
    });
    $OkButton.on('click', () => {
      todos.forEach((task) => {
        if (task.id === +id && $EditInput.val().trim()) {
          const currentTask = task;
          currentTask.text = $EditInput.val();
        }
      });
      drawList(todos);
    });

    $NoButton.on('click', () => {
      drawList(todos);
    });
  });

  // pagination changing
  $paginationBlock.on('click', (event) => {
    if (event.target.parentNode.className === 'current-page') {
      $('#pag li').removeClass('active');
      event.target.classList.add('active');
      const myPage = Number(event.target.innerHTML);
      countPages(myPage);
      page = myPage;
      drawList();
    }
  });

  // change page after selecting tab
  $currentDoc.on('click', '.btn.btn-outline-secondary', (event) => {
    classficateTabs(event);
    const btnId = $(event.target).attr('id');
    switch (btnId) {
      case 'show-all':
        page = Math.ceil(todos.filter((task) => task.done || !task.done).length / limitPerPage);
        countPages(page);
        tabStatus = 'all';
        break;
      case 'show-complete':
        page = Math.ceil(todos.filter((task) => task.done).length / limitPerPage);
        countPages(page);
        tabStatus = 'completed';
        break;
      case 'show-current':
        page = Math.ceil(todos.filter((task) => !task.done).length / limitPerPage);
        countPages(page);
        tabStatus = 'current';
        break;
      default:
        return false;
    }
    drawList();
    return true;
  });

  $currentDoc.on('click', '#bad', (event) => {
    const liId = $(event.target).closest('li').attr('id');
    changeStatus(liId);
  });

  // btn
  $RmCompleteButton.on('click', () => {
    removeCompleteTask();
  });

  // btn
  $CheckBtn.on('click', () => {
    changeCheckbox();
  });

  // btn
  $TaskInput.on('keypress', (event) => {
    if (event.key === 'Enter') {
      addTask();
    }
  });

  $AddBtn.on('click', () => {
    addTask();
  });
});
